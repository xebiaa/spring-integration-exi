package org.xebia.exi;

import static com.jayway.awaitility.Awaitility.*;
import static java.util.concurrent.TimeUnit.*;

import java.io.File;
import java.io.IOException;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;

import org.apache.commons.io.FileUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.MessageChannel;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author iwein
 */
@ContextConfiguration(locations = "/file-in-file-out.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class FileCycleTest {

    public static TemporaryFolder inputFolder = new TemporaryFolder();
    public static TemporaryFolder outputFolder = new TemporaryFolder();
    
    @Autowired MessageChannel exiOut;
 
    @BeforeClass
    public static void initializeTempFolders() throws IOException {
        inputFolder.create();
        outputFolder.create();
        Config.directories.put("inputFolder", inputFolder.getRoot().getPath());
        Config.directories.put("outputFolder", outputFolder.getRoot().getPath());
    }

    @AfterClass
    public static void removeTempFolders() throws IOException {
        inputFolder.delete();
        outputFolder.delete();
    }

    @Test
    public void shouldLoadContext() throws Exception {
        File inputFile = FileUtils.toFile(getClass().getClassLoader().getResource("input.xml"));
        FileUtils.copyFileToDirectory(inputFile, inputFolder.getRoot());
        await().atMost(5, SECONDS).until(outputFileIsPresent());
    }

    private Callable<Boolean> outputFileIsPresent() {
        return new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                File outputFile = new File(outputFolder.getRoot(), "input.xml");
                if (outputFile.exists()) {
                    return true;
                }
                return false;
            }
        };
    }

    public static class Config {
        public static final Map<String, String> directories = new HashMap<String, String>();
    }

}

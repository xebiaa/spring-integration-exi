package org.xebia.exi;

import java.io.IOException;
import java.io.OutputStream;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.commons.io.output.ByteArrayOutputStream;

import com.siemens.ct.exi.api.sax.EXIResult;
import com.siemens.ct.exi.exceptions.EXIException;



public class ExiReader {

    private static final Logger logger = LoggerFactory.getLogger(ExiReader.class);
    
    public Result read(Source xmlSource) throws TransformerException, IOException, EXIException {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();

        OutputStream out = new ByteArrayOutputStream();
        Result result = new EXIResult(out);
        transformer.transform(xmlSource, result);
        out.close();
        return result;
    }

}

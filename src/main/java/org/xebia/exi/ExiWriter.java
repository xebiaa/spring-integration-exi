package org.xebia.exi;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.xml.transform.sax.SAXResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import com.siemens.ct.exi.api.sax.EXIResult;
import com.siemens.ct.exi.exceptions.EXIException;


public class ExiWriter {

    private static final Logger logger = LoggerFactory.getLogger(ExiWriter.class);
    
    private final OutputStream outStream;
    
    public ExiWriter(OutputStream outStream) {
        this.outStream = outStream;
    }
    
    public void write(File inputFile) throws IOException, EXIException, SAXException {
        logger.info("Input file: " + inputFile.getPath());
        
        SAXResult exiResult = new EXIResult(outStream);
        XMLReader xmlReader = XMLReaderFactory.createXMLReader();
        xmlReader.setContentHandler(exiResult.getHandler());

        // parse xml file and send to input source.
        xmlReader.parse(new InputSource(new FileInputStream(inputFile)));
    }

}

These are the EXI adapters created as part of the [Xebia Next](http://next.xebia.com/) apprentice program.

[EXI](http://www.w3.org/XML/EXI/) is a new W3C recommendation that defines a binary standard for transmitting XML data.

Clasically, [XML information](http://www.w3.org/TR/2004/REC-xml-infoset-20040204/) is sent from one client to another using the classic XML text format. When transferring this XML information through a network, this XML text is usually passes through a compression algorithm, such as [GZip](http://en.wikipedia.org/wiki/Gzip), which reduces the size of the information passed through the line.

However, this approach has several problems, when considering using XML as an inter-process communication format:

1. Translating in-memory XML information to its ASCII-representation, and reversing that translation, causes overhead on both parties.
2. GZip is a generic compression algorithm, not specifically tailored or optimized for the well-defined, structured XML format.
3. GZip compression causes overhead, both during compression and decompression.

EXI (Efficient XML Interchange) attempts to solve all of these shortcomings. In summary, EXI is a binary standard for efficiently transmitting XML information. It does this by translating an XML document into a set of events, not unlike those used in the [SAX](http://sax.sourceforge.net/) API, representing those events as short, binary 'commands'. Next to that, smart compression techniques are used to reduce the information as much as possible.

For a more detailed explanation of EXI, please read the [EXI Primer](http://www.w3.org/TR/2009/WD-exi-primer-20091208/) on W3.org.

Building
========

Currently, we use the EXIFicient (TODO: link) library to convert an XML representation to EXI. This is a non-Maven project, and isn't found in any public repositories as of yet. To build the project, you first need to import the exificient.jar file into your local maven repository using:

mvn install:install-file -DgroupId=com.siemens.ct.exi -DartifactId=exificient -Dversion=0.7 -Dpackaging=jar -Dfile=exificient.jar -Dsources=exificient-0.7-src.zip -Djavadoc=exificient-0.7-src.zip

And also add Xerces (version unknown) and xml-apis (version unknown) to the Maven repo from the lib directory.

TODO: fix this dependency hell.

mvn install:install-file -DgroupId=xml-apis -DartifactId=xml-apis -Dversion=apis-exificient -Dpackaging=jar -Dfile=lib/xml-apis.jar

mvn install:install-file -DgroupId=xerces -DartifactId=xercesImpl -Dversion=xerces-exificient -Dpackaging=jar -Dfile=lib/xercesImpl.jar